<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator,Redirect,Response;
Use App\User;
Use App\Categories;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class BackendController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    public function getContent()
    {
        if(Auth::check()){
        return view('backend/content/content');
        }else{
            return redirect('/');
          }
    }
    public function getLogout()
    {
        Auth::logout();
        return Redirect::to('/');
    }
    public function postLogin(Request $request)
    
        {
            request()->validate([
            'username' => 'required',
            'password' => 'required',
            ]);
     
            $credentials = $request->only('username', 'password');
            if (Auth::attempt($credentials)) {
                // Authentication passed...
                return redirect()->intended('backend');
            }
            return Redirect::to("login")->withSuccess('Oppes! You have entered invalid credentials');
        
      

    }
    public function getAdd($type)
    {
        if(Auth::check()){
            switch($type)
            {
                case 'content':
                    $datacategories = Categories::orderBy('id','desc')->get();
                    return view('backend/content/addcontent',['categories'=>$datacategories]);
               break;
                
            }
        }else{
            return redirect('/');
          }
}
}