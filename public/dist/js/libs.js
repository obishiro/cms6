$(document).ready(function() {
    
    $('#data-content').DataTable({
      "pageLength": 50,
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    $('#menu-close').click(function (){
      Swal.fire({
        title: 'ต้องการปิดระบบจริงหรือไม่?',
        text: "กรุณายืนยันการปิดระบบ",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText:  'ยกเลิก',
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            
            ' ออกจากระบบ!',
             
            window.location = "logout"
          )
        }
      })
       
    });
  });