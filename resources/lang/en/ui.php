<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Backend_title' =>  'ระบบบริหารจัดการเนื้อหา',
    'login_input_title' => 'กรอกข้อมูลเพื่อเข้าสู่ระบบ!',
    'username'  =>  'ชื่อผู้ใช้งาน',
    'password'  =>  'รหัสผ่าน',
    'bt-login'  =>  'เข้าสู่ระบบ',
    'bt-save'   =>  'บันทึกข้อมูล',
    'bt-del'    =>  'ลบ',
    'bt-add'    =>  'เพิ่มข้อมูล',
    'bt-uploads' => 'อัพโหลด',
    'choose-file'   => 'เลือกไฟล์',
    'files-input'         => 'ไฟล์/เอกสาร (ถ้ามี)',
    'files-images'         => 'รูปภาพและไฟล์/เอกสาร',
    'images'                => 'รูปภาพ (ถ้ามี)',
    'bt-cancel' =>  'ยกเลิก',
    'pls-input' =>  'กรุณากรอกข้อมูลในช่อง',
    // backend ui
    'config-env-title'  =>  'ตั้งค่าระบบ',
    'config-env-basic'  =>  'ข้อมูลพื้นฐานระบบ',
    'config-env-slide'  =>  'ข้อมูลป้ายสไลด์',
    'config-menu-title' =>  'ตั้งค่าระบบเมนู',
    'config-menu-main'  =>  'ระบบเมนูหลัก',  
    'config-menu-sub'   =>  'ระบบเมนูย่อย',  
    'config-content-title'  =>  'ระบบข่าวสาร',
    'config-content-add'    =>  'เขียนข่าวสาร',
    'config-content-main'  =>  'ระบบหมวดหมู่ข่าวสาร',  
    'config-content-sub'   =>  'ระบบข่าวสาร',
    'config-content-main'  =>  'ระบบหมวดหมู่ข่าวสาร',
    'config-content-sub'  =>  'ระบบข่าวสาร',
    'config-gallery-title'  =>  'ระบบคลังภาพ',
    'config-user-title'     =>  'ระบบผู้ใช้งาน',
    'config-logout'         =>  'ปิดระบบ',
    'config-choose'         => '-- เลือก --',

    // content ui
    'content-category'  =>  'หมวดหมู่ข่าวสาร',
    'content-name'  => 'หัวข้อข่าวสาร',
    'content-detail'    => 'รายละเอียดเบืองต้น',
    'content-all-detail'    => 'รายละเอียดทั้งหมด',
     


  
];
