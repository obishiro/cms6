@extends('backend/master')
@section('content')
     
      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
          
          <!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-8">
             
                          <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> <i class="nav-icon fas fa-edit"></i> @lang('ui.config-content-add')</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form  method="post" id="upload_form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputPassword1">@lang('ui.content-category')</label>
                    <select class="form-control" name="content-category">
                      <option value="">@lang('ui.config-choose')</option>
                      @foreach ($categories as $c)
                        <option value="{{$c->id}}">{{$c->categories_name}}</option>
                       @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">@lang('ui.content-name')</label>
                    <input type="text" name="content-name" class="form-control" id="" placeholder="@lang('ui.pls-input')">
                  </div>
                  <div class="form-group">
                    <label for="">@lang('ui.content-detail')</label>
                    <textarea name="content-detail" class="content-detail" placeholder="@lang('ui.pls-input')"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="">@lang('ui.content-all-detail')</label>
                    <textarea name="content-all-detail" class="content-all-detail" placeholder="@lang('ui.pls-input')"></textarea>
                  </div>
                 
          
                <!-- /.card-body -->

              
            </div>

            </section>
            <section class="col-lg-4">
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title"><i class="fas fa-file-image"></i> @lang('ui.files-images')</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <div class="form-group">
                    <div class="alert" id="message" style="display: none"></div>
                    <span id="uploaded_image"></span>
                    <label for="exampleInputFile">@lang('ui.images')</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="img-name" id="img-name" class="custom-file-input">
                        <label class="custom-file-label" for="exampleInputFile">@lang('ui.choose-file')</label>
                      </div>
                      <div class="input-group-append">
                        <button type="button" class="btn btn-primary" id="uploads-img"><i class="fas fa-upload"></i> @lang('ui.bt-uploads')</button>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">@lang('ui.files-input')</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" id="file-name" class="custom-file-input">
                        <label class="custom-file-label" for="exampleInputFile">@lang('ui.choose-file')</label>
                      </div>
                      <div class="input-group-append">
                        <button type="button" class="btn btn-primary" id="uploads-files"><i class="fas fa-upload"></i> @lang('ui.bt-uploads')</button>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="card-footer">
                  <div class="col-lg-12 float-right">
                  <button type="submit" class="btn btn-success">  <i class="fas fa-plus-circle"></i> @lang('ui.bt-add')</button>
          

                  <button type="reset" class="btn btn-danger float-right"> <i class="fas fa-power-off"></i> @lang('ui.bt-cancel')</button>
                </div>
                </div>
              </div>
             
                 
            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            
            <!-- right col -->
          </div>
          <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
        <input type="hidden" name="key" value="{{ Str::random(16,'numberic') }}" >
      </form>
      </section>
@endsection
@section('script')
<script src="{{ URL::to('plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<script>

 
  $(function () {
    // Summernote
    $('.content-detail').summernote({
      height: 200,
    })
    $('.content-all-detail').summernote({
      height: 350,
    })
  })
  bsCustomFileInput.init();
  
</script>
<script>
  $(document).ready(function(){
    $.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});
   $('#uploads-img').on('click', function(event){
    var formData = new FormData();
    //formData.append('file', $('[name=img-name]')[0].files[0]);
    formData.append('file', $('input[type=file]')[0].files[0]);
    
    event.preventDefault();
    $.ajax({
     url:"{{ route('content.addimg') }}",
     method:"POST",
     //data:new FormData(this),
     data:formData,
     dataType:'JSON',
     contentType: false,
     cache: false,
     processData: false,
     success:function(data)
     {
      $('#message').css('display', 'block');
      $('#message').html(data.message);
      $('#message').addClass(data.class_name);
      $('#uploaded_image').html(data.uploaded_image);
     }
    })
   });
  
  });
  </script>
 
    
@endsection