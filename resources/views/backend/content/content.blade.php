@extends('backend/master');
@section('content')
     
      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
          
          <!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
             
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title"> <i class="fas fa-copy"></i> @lang('ui.config-content-title')</h3>
                  <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 200px;">
                     
  
                    <a href="{{ URL::to('backend/add/content')}}" class="btn btn-success btn-lg active" role="button" aria-pressed="true"><i class="nav-icon fas fa-edit"></i> @lang('ui.config-content-add')</a>
                    </div>
                  </div>
                </div>
                
                 
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="data-content" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Rendering engine</th>
                      <th>Browser</th>
                      <th>Platform(s)</th>
                      <th>Engine version</th>
                      <th>CSS grade</th>
                    </tr>
                    </thead>
                    <tbody>
                 
                    <tr>
                      <td>Misc</td>
                      <td>PSP browser</td>
                      <td>PSP</td>
                      <td>-</td>
                      <td>C</td>
                    </tr>
                    <tr>
                      <td>Other browsers</td>
                      <td>All others</td>
                      <td>-</td>
                      <td>-</td>
                      <td>U</td>
                    </tr>
                    </tbody>
                    
                  </table>
                </div>
                <!-- /.card-body -->
              </div>

            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            
            <!-- right col -->
          </div>
          <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
      </section>
@endsection
@section('script')
<script src="{{URL::to('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
 
 
    
@endsection