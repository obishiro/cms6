<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@lang('ui.Backend_title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL::to('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ URL::to('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::to('dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{{ URL::to('index2.html')}}"><b>@lang('ui.Backend_title')</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">@lang('ui.login_input_title')</p>

      <form action="{{ url('backend/login') }}" method="POST">
        @csrf
        @if ($errors->has('username'))
      <span class="text-danger">@lang('ui.pls-input')</span>
        @endif
        <div class="input-group mb-3">
          <input type="text" class="form-control @if ($errors->has('username')) is-invalid @endif" name="username" placeholder="@lang('ui.username')">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user-circle"></span>
            </div>
          </div>
        </div>
        @if ($errors->has('password'))
        <span class="text-danger">@lang('ui.pls-input')</span>
          @endif
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control @if ($errors->has('password')) is-invalid @endif" placeholder="@lang('ui.password')">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
          
              <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-sign-out-alt"></i> @lang('ui.bt-login')</button>
           
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="reset" class="btn btn-danger btn-block"><i class="fas fa-power-off"></i> @lang('ui.bt-cancel')</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

     

    
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ URL::to('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ URL::to('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::to('dist/js/adminlte.min.js')}}"></script>

</body>
</html>
