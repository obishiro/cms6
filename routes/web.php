<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('login',function(){
    return view('backend.login');
});
Route::post('backend/login', 'BackendController@postLogin');
Route::group(array('prefix'=>'backend','before' => 'auth'), function(){
    Route::get('/','BackendController@getContent');
   // Route::get('dashboard','BackendController@getContent');
    Route::get('logout','BackendController@getLogout');
    Route::get('content','BackendController@getContent');
    Route::get('add/{type}','BackendController@getAdd')->where(array('type' =>'[A-Za-z]+'));
    Route::post('content/addimg', 'ContentController@addimg')->name('content.addimg');
 
});